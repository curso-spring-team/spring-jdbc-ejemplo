package pe.com.tutoriales.springjdbcejemplo.dao;

import static org.junit.Assert.*;

import java.security.Principal;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pe.com.tutoriales.springjdbcejemplo.model.Sucursal;
import pe.com.tutoriales.springjdbcejemplo.storedprocedure.mapper.SucursalMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class jdbcTemplateTest {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	DataSource dataSource;
	
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate ;
	

	@Test
	@Ignore
	public void queryConParametros() {

		String sql = "SELECT * FROM SUCURSAL WHERE codigo=? ";
		 
		List<Sucursal> listaSucursales= this.jdbcTemplate.query(sql, new Object[]{"MIRA01"} ,new BeanPropertyRowMapper(Sucursal.class));
		
		for (Sucursal sucursal : listaSucursales) {
			System.out.println(sucursal);
		}
	}
	
	@Test
	@Ignore
	public void queryConParametrosUnSoloObjeto() {

		String sql = "SELECT * FROM SUCURSAL WHERE codigo=? ";
		 
		Sucursal sucursal= this.jdbcTemplate.queryForObject(sql, new Object[]{"MIRA01"} ,new BeanPropertyRowMapper(Sucursal.class));
		
		
			System.out.println(sucursal);
		
	}
	
	@Test
	//@Ignore
	public void namedParameterJdbcTemplate() {

		String sql = "SELECT * FROM SUCURSAL WHERE codigo= :codigo ";
		
		 SqlParameterSource namedParameters = new MapSqlParameterSource("codigo", "MIRA01");
		 
		 
		Sucursal sucursal= this.namedParameterJdbcTemplate.queryForObject(sql, namedParameters ,new BeanPropertyRowMapper(Sucursal.class));
		
		
		System.out.println(sucursal);
		
	}
	
	
	
	
	
	
	@Test
	@Ignore
	public void insert() {

		Sucursal sucursal=new Sucursal();
		sucursal.setIdSucursal(10);
		sucursal.setNombre("otra sucursal");
		sucursal.setCodigo("codigo");
		
		String sql = "INSERT INTO SUCURSAL (ID_SUCURSAL,NOMBRE,CODIGO) VALUES (?,?,?) ";
		
		SqlParameterSource parameters = new BeanPropertySqlParameterSource(sucursal);
		
		this.jdbcTemplate.update(sql,new Object[] {sucursal.getIdSucursal(),sucursal.getNombre(),sucursal.getCodigo()});
		
		
	}
	
	@Test
	@Ignore
	public void update() {

		Sucursal sucursal=new Sucursal();
		sucursal.setIdSucursal(10);
		sucursal.setCodigo("codigoModificado");
		String sql = "UPDATE SUCURSAL SET CODIGO=? WHERE ID_SUCURSAL =?";
		
		SqlParameterSource parameters = new BeanPropertySqlParameterSource(sucursal);
		
		this.jdbcTemplate.update(sql,new Object[] {sucursal.getCodigo(),sucursal.getIdSucursal()});
		
		
	}
	
	@Test
	@Ignore
	public void delete() {

		Sucursal sucursal=new Sucursal();
		sucursal.setIdSucursal(10);
		String sql = "DELETE SUCURSAL WHERE ID_SUCURSAL=?";
		
		SqlParameterSource parameters = new BeanPropertySqlParameterSource(sucursal);
		
		this.jdbcTemplate.update(sql,new Object[] {sucursal.getIdSucursal()});
		
		
	}

}
