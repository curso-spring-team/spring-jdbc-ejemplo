package pe.com.tutoriales.springjdbcejemplo.dao;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pe.com.tutoriales.springjdbcejemplo.model.Sucursal;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class SucursalDAOImplTest {
	
	
	@Autowired
	private SucursalDAO sucursalDAO;
	
	@Test
	//@Ignore
	public void listarSucursalesCorrectaStoreProcedure(){
		List<Sucursal> listaSucursales=new ArrayList<Sucursal>();
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
			listaSucursales=sucursalDAO.findStoreProcedure(null,null);
		}
	}
	
	
	@Test
	//@Ignore
	public void listarSucursalesCorrectaSimpleJdbcCall(){
		List<Sucursal> listaSucursales=new ArrayList<Sucursal>();
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
			listaSucursales=sucursalDAO.findSimpleJdbcCall(null,null);
		}
	}
	
	@Test
	//@Ignore
	public void listarSucursalesCorrectaSimpleJdbcCallNuevaInstancia(){
		List<Sucursal> listaSucursales=new ArrayList<Sucursal>();
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
			listaSucursales=sucursalDAO.findSimpleJdbcCallNuevaInstancia(null,null);
		}
	}
	
	
	
	@Test
	//@Ignore
	public void listarSucursalesCorrectaJdbcTemplate(){
		List<Sucursal> listaSucursales=new ArrayList<Sucursal>();
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
			listaSucursales=sucursalDAO.findJdbcTemplate(null,null);
		}
	}
	
	

}
