package pe.com.tutoriales.springjdbcejemplo.dao.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pe.com.tutoriales.springjdbcejemplo.dao.SucursalDAO;
import pe.com.tutoriales.springjdbcejemplo.model.Sucursal;
import pe.com.tutoriales.springjdbcejemplo.storedprocedure.ConsultarSucursalStoredProcedure;


@Repository
public  class  SucusalDAOImpl implements SucursalDAO {
	
	@Autowired
	private DataSource dataSource;
	
	
	@Autowired JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall jdbcCall;

	@Autowired
	private ConsultarSucursalStoredProcedure consultarSucursalStoredProcedure;
	
	public SucusalDAOImpl() {
		super();
	}
	
	@PostConstruct
	public void init(){
		jdbcCall=new SimpleJdbcCall(this.dataSource);
	}



	
	public List<Sucursal> findStoreProcedure(String nombre,String codigo) {

		/*Map<String, Object> parametros =new HashMap<String, Object>();
		parametros.put("IN_NOMBRE", nombre);
		parametros.put("IN_CODIGO", codigo);*/
		Map<String, Object> result=this.consultarSucursalStoredProcedure.execute(nombre,codigo);
		List<Sucursal> sucursales=(List<Sucursal>)result.get("OUT_SUCURSALES");

		return sucursales;
	}
	
	
	public List<Sucursal> findSimpleJdbcCall(String nombre,String codigo) {

	
		jdbcCall.withCatalogName("PKG_SUCURSAL");
		jdbcCall.withProcedureName("FIND");
		jdbcCall.addDeclaredParameter(new SqlParameter("IN_NOMBRE", OracleTypes.VARCHAR));
		jdbcCall.addDeclaredParameter(new SqlParameter("IN_CODIGO", OracleTypes.VARCHAR));
		jdbcCall.addDeclaredParameter(new SqlOutParameter("OUT_SUCURSALES", OracleTypes.CURSOR,new BeanPropertyRowMapper(Sucursal.class)));
		MapSqlParameterSource parametros = new MapSqlParameterSource();
		parametros.addValue("IN_NOMBRE", nombre);
		parametros.addValue("IN_CODIGO", codigo);
	
		Map<String,Object> results = jdbcCall.execute(parametros);
		return (List<Sucursal>) results.get("OUT_SUCURSALES");
	}
	
	
	public List<Sucursal> findSimpleJdbcCallNuevaInstancia(String nombre,String codigo) {

		jdbcCall=new SimpleJdbcCall(jdbcTemplate.getDataSource());
		jdbcCall.withCatalogName("PKG_SUCURSAL");
		jdbcCall.withProcedureName("FIND");
		jdbcCall.addDeclaredParameter(new SqlParameter("IN_NOMBRE", OracleTypes.VARCHAR));
		jdbcCall.addDeclaredParameter(new SqlParameter("IN_CODIGO", OracleTypes.VARCHAR));
		jdbcCall.addDeclaredParameter(new SqlOutParameter("OUT_SUCURSALES", OracleTypes.CURSOR,new BeanPropertyRowMapper(Sucursal.class)));
		//jdbcCall.addDeclaredParameter(new SqlOutParameter("P_OUT_LISTA_SUCURSAL", OracleTypes.CURSOR,new SucursalMapper()));
		MapSqlParameterSource parametros = new MapSqlParameterSource();
		parametros.addValue("IN_NOMBRE", nombre);
		parametros.addValue("IN_CODIGO", codigo);
	
		Map<String,Object> results = jdbcCall.execute(parametros);
		return (List<Sucursal>) results.get("OUT_SUCURSALES");
	}
	
	
	
	
	
	public List<Sucursal> findJdbcTemplate(String nombre,String codigo) {

	
		String sql = "SELECT * FROM SUCURSAL ";
		 
		List<Sucursal> listaSucursales= this.jdbcTemplate.query(
				sql,  new BeanPropertyRowMapper(Sucursal.class));
		
		
		return listaSucursales;
	}
	
	public List<Sucursal> findJdbcTemplateQueryForObject(String nombre,String codigo) {

		
		//String sql = "SELECT * FROM SUCURSAL like '%'|| ? ||'%' ";
		String sql = "SELECT * FROM SUCURSAL ";
		 
		List<Sucursal> listaSucursales= this.jdbcTemplate.queryForList(sql,Sucursal.class);
				
		
		
		return listaSucursales;
	}
	
	
}
