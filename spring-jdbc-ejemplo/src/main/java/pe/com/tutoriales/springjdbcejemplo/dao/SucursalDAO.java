package pe.com.tutoriales.springjdbcejemplo.dao;

import java.util.List;

import pe.com.tutoriales.springjdbcejemplo.model.Sucursal;



public interface SucursalDAO {
	
	public abstract List<Sucursal> findStoreProcedure(String nombre,
			String codigo);

	public abstract List<Sucursal> findSimpleJdbcCall(String nombre,
			String codigo);

	public abstract List<Sucursal> findSimpleJdbcCallNuevaInstancia(
			String nombre, String codigo);

	public abstract List<Sucursal> findJdbcTemplate(String nombre, String codigo);
	
	public abstract List<Sucursal> findJdbcTemplateQueryForObject(String nombre,String codigo);


}
