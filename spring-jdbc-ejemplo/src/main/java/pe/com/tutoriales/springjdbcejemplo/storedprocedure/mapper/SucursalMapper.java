package pe.com.tutoriales.springjdbcejemplo.storedprocedure.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import pe.com.tutoriales.springjdbcejemplo.model.Sucursal;



public final class SucursalMapper implements RowMapper<Sucursal> {

    public Sucursal mapRow(ResultSet rs, int rowNum) throws SQLException {
    	Sucursal sucursal = new Sucursal();
    	sucursal.setIdSucursal(rs.getInt("ID_SUCURSAL"));
    	sucursal.setNombre(rs.getString("NOMBRE"));
    	sucursal.setCodigo(rs.getString("CODIGO"));
        return sucursal;
    }
}
