package pe.com.tutoriales.springjdbcejemplo.storedprocedure;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.junit.runners.Parameterized.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import pe.com.tutoriales.springjdbcejemplo.model.Sucursal;


@Component
public class ConsultarSucursalStoredProcedure extends StoredProcedure {

    private static final String SPROC_NAME = "PKG_SUCURSAL.FIND";
   
	@Autowired
	public ConsultarSucursalStoredProcedure(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        System.out.println("ConsultarSucursalStoredProcedure");
        declareParameter(new SqlParameter("IN_NOMBRE", OracleTypes.VARCHAR));
        declareParameter(new SqlParameter("IN_CODIGO", OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter("OUT_SUCURSALES", OracleTypes.CURSOR, new BeanPropertyRowMapper(Sucursal.class)));
       
        compile();
       
    }

	
    public Map<String, Object> execute(String nombre,String codigo) {
    
    	Map<String, Object> parametros =new HashMap<String, Object>();
		parametros.put("IN_NOMBRE", nombre);
		parametros.put("IN_CODIGO", codigo);
        return super.execute(parametros);
    }
}
